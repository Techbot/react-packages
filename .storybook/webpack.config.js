// load the default config generator.
const genDefaultConfig = require('@storybook/react/dist/server/config/defaults/webpack.config.js')
const { exec, execSync } = require('child_process')
const Dotenv = require('dotenv-webpack')
const path = require('path')

module.exports = (baseConfig, env) => {
  const config = genDefaultConfig(baseConfig, env)
  const { bail, plugins } = config

  // add dotenv files
  plugins.push(new Dotenv({ path: path.resolve('.env.local') }))
  plugins.push(new Dotenv({ path: path.resolve('.env') }))

  if (bail) {
    execSync('yarn build', { stdio: [0, 1, 2] })
  } else {
    exec('yarn watch', { stdio: [0, 1, 2] })
  }

  return config
}
