# @4geit/rct-header-component [![npm version](//badge.fury.io/js/@4geit%2Frct-header-component.svg)](//badge.fury.io/js/@4geit%2Frct-header-component)

---

header component used as part of a generic layout component

## Demo

A live storybook is available to see how the component looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-header-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-header-component) package manager using the following command:

```bash
npm i @4geit/rct-header-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-header-component
```

2. Depending on where you want to use the component you will need to import the class `RctHeaderComponent` to your project JS file as follows:

```js
import RctHeaderComponent from '@4geit/rct-header-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctHeaderComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctHeaderComponent from '@4geit/rct-header-component'
// ...
const App = () => (
  <div className="App">
    <RctHeaderComponent/>
  </div>
)
```
