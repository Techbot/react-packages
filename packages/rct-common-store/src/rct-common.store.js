import { observable, action, reaction } from 'mobx'

class RctCommonStore {
  @observable appName = 'Test'
  @observable token = window.localStorage.getItem('token')
  @observable firstname
  @observable lastname
  @observable isLoggedIn = false
  @observable appLoaded = false

  constructor() {
    reaction(
      () => this.token,
      token => {
        if (token) {
          window.localStorage.setItem('token', token)
        } else {
          window.localStorage.removeItem('token')
        }
      }
    )
  }

  @action setToken(token) {
    this.token = token
    this.isLoggedIn = true
  }
  @action setFirstname(firstname) {
    this.firstname = firstname
  }
  @action setLastname(firstname) {
    this.lastname = lastname
  }
  @action setAppLoaded() {
    this.appLoaded = true
  }
  @action logout() {
    window.localStorage.removeItem('token')
    this.token = undefined
    this.isLoggedIn = false
  }
}

export default new RctCommonStore()
