import { observable, action, runInAction, toJS } from 'mobx'
import { hashHistory } from 'react-router'

class RctDatePickerStore {
  @observable date = null
  @observable focused = false

  @action setDate(value) {
    this.date = value
  }
  @action setFocused(value) {
    this.focused = value
  }
}

export default new RctDatePickerStore()
