import { observable, action, computed, runInAction, toJS } from 'mobx'
import { hashHistory } from 'react-router'
import moment from 'moment-timezone'

import swaggerClientStore from '@4geit/rct-swagger-client-store'
import notificationStore from '@4geit/rct-notification-store'
import datePickerStore, { date } from '@4geit/rct-date-picker-store'

class RctEventListStore {
  @observable inProgress = false
  @observable page = 0
  @observable data = []
  @computed get sortedData() {
    const group = this.data.reduce((obj, item) => {
      const { startTime, timeZone } = item
      const date = moment(startTime).tz(timeZone).format('YYYY-MM-DD')
      obj[date] = obj[date] || []
      obj[date].push(item)
      return obj
    }, Object.create(null))
    return Object.entries(group).map(([date, timeslots]) => ({
      date,
      timeslots: timeslots.slice().sort( (a, b) => moment(a.startTime).diff(moment(b.startTime), 'minutes'))
    }))
  }
  getNumberOfWeeks(start, end) {
    const endDate = moment(end)
    const startDate = moment(start)
    return endDate.diff(startDate, 'week')
  }
  getNextEvent(start, week, day) {
    const startTimeWeek = moment(start).add(week, 'week')
    return moment(startTimeWeek).day(day + 1).tz('UTC').format('YYYY-MM-DDTHH:mm:ss.000\\Z')
  }
  @action appendData(value) {
    this.data = this.data || []
    this.data = this.data.concat(value)
    this.page++;
    console.log(this.data)
  }
  @action reset() {
    this.data = []
    this.page = 0
  }
  @action async fetchData({ operationId }) {
    this.inProgress = true
    try {
      console.log('fetchData')
      const { body: { list } } = await swaggerClientStore.client.apis.Account[operationId]({
        pageSize: 10,
        page: this.page,
        // start date range
        'dateRange[]': (datePickerStore.date && datePickerStore.date.format('YYYY-MM-DD')) || '',
        // disabled since the end date range is not working on the API
        // 'dateRange[]': (datePickerStore.date && datePickerStore.date.format('YYYY-MM-DD')) || moment().month(moment().month() + 1).format('YYYY-MM-DD'),
        // active status only
        status: 'active',
      })
      if (list && list.length) {
        const toAdd = []
        const { startTime: lastStartTime } = list.slice(-1)
        list.map( timeslot => {
          const { eventId, activity, events, title, startTime, timeZone, daysRunning, untilTime } = timeslot
          const { color } = activity
          if (!daysRunning.length) {
            toAdd.push(timeslot)
            return
          }
          const numberOfWeeks = this.getNumberOfWeeks(startTime, lastStartTime)
          console.log('numberOfWeeks')
          console.log(numberOfWeeks)
          Array.from(Array(numberOfWeeks).keys()).map( week => {
            daysRunning.map( day => {
              if (!events.length) {
                const newStartTime = this.getNextEvent(startTime, week, day)
                toAdd.push({eventId, title, timeZone, startTime: newStartTime, activity, events})
                console.log('newStartTime')
                console.log(newStartTime)
              }
              const isMatching = events.some(({ eventInstanceId, title, timeZone, startTime })  => {
                const calcEventInstanceId = `${eventId}_${moment(startTime).days(day).tz('UTC').format('YYYYMMDDTHHmmss\\Z')}`
                if ( eventInstanceId === calcEventInstanceId ) {
                  toAdd.push({eventId, title, timeZone, startTime, activity, events})
                  return true
                }
              })
              if (!isMatching) {
                toAdd.push(timeslot)
              }
            })
          })
        })
        runInAction(() => {
          console.log('toAdd')
          console.log(toAdd)
          this.appendData(toAdd)
          this.inProgress = false
        })
      }
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
}

export default new RctEventListStore()
