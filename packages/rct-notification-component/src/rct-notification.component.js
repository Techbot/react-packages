import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import compose from 'recompose/compose'
import classNames from 'classnames'
import Snackbar from 'material-ui/Snackbar'

import './rct-notification.component.css'

@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  // TBD
}))
@withWidth()
@inject('notificationStore')
@observer
@propTypes({
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  // TBD
})
@defaultProps({
  // TBD
})
export default class RctNotificationComponent extends Component {
  handleSnackbar() {
    this.props.notificationStore.close()
  }
  render() {
    const { open, message, duration } = this.props.notificationStore
    return (
      <Snackbar
        open={ open }
        message={ message || 'no message' }
        autoHideDuration={ duration }
        onRequestClose={ this.handleSnackbar.bind(this) }
      />
    )
  }
}
