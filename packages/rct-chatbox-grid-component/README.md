# @4geit/rct-chatbox-grid-component [![npm version](//badge.fury.io/js/@4geit%2Frct-chatbox-grid-component.svg)](//badge.fury.io/js/@4geit%2Frct-chatbox-grid-component)

---

chatbox grid component

## Demo

A live storybook is available to see how the component looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-chatbox-grid-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-chatbox-grid-component) package manager using the following command:

```bash
npm i @4geit/rct-chatbox-grid-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-chatbox-grid-component
```

2. Depending on where you want to use the component you will need to import the class `RctChatboxGridComponent` to your project JS file as follows:

```js
import RctChatboxGridComponent from '@4geit/rct-chatbox-grid-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctChatboxGridComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctChatboxGridComponent from '@4geit/rct-chatbox-grid-component'
// ...
const App = () => (
  <div className="App">
    <RctChatboxGridComponent/>
  </div>
)
```
